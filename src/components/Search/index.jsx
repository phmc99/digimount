import { useState } from "react";
import { useDispatch } from "react-redux";
import addDigimonsThunk from "../../store/modules/digimons/thunks";

const Search = () => {
  const [userInput, setUserInput] = useState("");

  const dispatch = useDispatch();

  const handleSearch = () => {
    dispatch(addDigimonsThunk(userInput));
    setUserInput("");
  };

  return (
    <div>
      <input
        value={userInput}
        onChange={(e) => setUserInput(e.target.value)}
        type="text"
        placeholder="Procure seu Digimon"
      />
      <button onClick={handleSearch}>Pesquisar</button>
    </div>
  );
};

export default Search;

import { useSelector } from "react-redux";

const Digimons = () => {
  const { digimons } = useSelector((state) => state);

  return (
    <div>
      <div>
        <span>{digimons.name}</span>
        <br />
        <img
          src={digimons.img}
          alt={digimons.name}
          style={{ width: "120px" }}
        />
      </div>
    </div>
  );
};

export default Digimons;

import axios from "axios";

import { addDigimon } from "./actions";

const addDigimonsThunk = (digimon) => (dispatch) => {
  axios
    .get("https://digimon-api.vercel.app/api/digimon")
    .then((response) =>
      dispatch(addDigimon(response.data.find((item) => item.name === digimon)))
    );
};

export default addDigimonsThunk;
